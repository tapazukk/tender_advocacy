![auth](auth-screenshot.png)

### Init
-	`npm install`

### Run server 
- `gulp`
- go to http://localhost:3000/

### Static result
- Look in the folder `build` 😬 

### Web version
- [auth pahe](https://tapazukk.gitlab.io/tender_advocacy/)
- [order-list](https://tapazukk.gitlab.io/tender_advocacy/order-list.html)